import requests
from bs4 import BeautifulSoup
import time
import smtplib



""" Основной класс """


class Currency:
	DOLLAR_UAH = 'https://www.google.com/search?q=dollar+to+uah&sxsrf=AJOqlzXrHvhiMI-AUHIPZK11637-eTbebA%3A1677514315120&ei=S9b8Y7LyBuiPwPAPsI2ZwAo&oq=dollar+&gs_lcp=Cgxnd3Mtd2l6LXNlcnAQAxgBMgkIIxAnEEYQggIyBAgAEEMyBggAEAoQQzIECAAQQzIECAAQQzIECAAQQzILCAAQgAQQsQMQgwEyBQgAEIAEMgUIABCABDIFCAAQgAQ6BAgjECc6CgguEMcBENEDEEM6CAgAELEDEIMBOggIABCABBCxAzoICC4QgAQQsQNKBAhBGABQAFjuC2CJI2gAcAF4AIABf4gB-QWSAQMwLjeYAQCgAQHAAQE&sclient=gws-wiz-serp'
	headers = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36'}

	current_converted_price = 0
	difference = 2 # Разница после которой будет отправлено сообщение на почту

	def __init__(self):
		# Установка курса валюты при создании объекта
		self.current_converted_price = float(self.get_currency_price().replace(",", "."))


	"""Метод для получения курса валюты """

	def get_currency_price(self):
		# Парсим всю страницу
		full_page = requests.get(self.DOLLAR_UAH, headers=self.headers)

		# Разбираем через BeautifulSoup
		soup = BeautifulSoup(full_page.content, 'html.parser')

		# Получаем нужное для нас значение и возвращаем его
		convert = soup.findAll("span", {"class": "DFlfde", "class": "SwHCTb", "data-precision": 2})
		return convert[0].text

	# Проверка изменения валюты
	def check_currency(self):
		currency = float(self.get_currency_price().replace(",", "."))
		if currency >= self.current_converted_price + self.difference:
			print("Курс вырос")
			self.send_mail()
		elif currency <= self.current_converted_price - self.difference:
			print("Курс упал")
			self.send_mail()

		print("Сейчас курс: 1 доллар = " + str(currency))
		time.sleep(3) # Засыпание программы на 3 секунды
		self.check_currency()


	""" Отправка почты через SMTP """

	def send_mail(self):
		server = smtplib.SMTP('smtp.gmail.com', 587)
		server.ehlo()
		server.starttls()
		server.ehlo()

		server.login('ВАША ПОЧТА', 'СГЕНЕРИРУЕМЫЙ ПАРОЛЬ')

		subject = 'Тема письма'
		body = 'Тело письма'
		message = f'Subject: {subject}\n{body}'

		server.sendmail(
			'От кого',
			'Кому',
			message
		)
		server.quit()

# Создание объекта и вызов метода
currency = Currency()
currency.check_currency()